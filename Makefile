.PHONY: test
test:
	py.test

.PHONY: black
black:
	black fileops

.PHONY: freeze
freeze:
	pip freeze > requirements.txt

.PHONY: install
install:
	pip install -r requirements.txt

.PHONY: build
build:
	python setup.py sdist bdist_wheel

.PHONY: test-upload
test-upload:
	python -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

.PHONY: test-publish
test-publish: build test-upload