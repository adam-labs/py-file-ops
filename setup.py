import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="file-ops-pkg-adam-labs",
    version="0.0.2",
    author="Adam Labruyere",
    author_email="adam.labs.info@gmail.com",
    license="MIT",
    description="File operations toolset package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/adam-labs/py-file-ops/src/master/",
    packages=setuptools.find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
