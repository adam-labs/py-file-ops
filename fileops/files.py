import os
import re


class File:
    def __init__(self, file_path):
        self.file_path = file_path

    @property
    def path(self):
        return os.path.dirname(self.file_path)

    @property
    def filename(self):
        return os.path.basename(self.file_path)

    @property
    def base_name(self):
        return os.path.splitext(self.filename)[0]

    @property
    def extension(self):
        return os.path.splitext(self.filename)[1].lower().strip(".")

    def replace_non_alphanumeric(self, subs="_"):
        new_name = re.sub("[^0-9a-zA-Z]+", subs, self.base_name) + "." + self.extension
        dst = os.path.join(self.path, new_name)
        os.rename(self.file_path, dst)
        self.file_path = dst
        return self

    def add_suffix(self, suffix):
        new_name = self.base_name + suffix + "." + self.extension
        dst = os.path.join(self.path, new_name)
        os.rename(self.file_path, dst)
        self.file_path = dst
        return self

    def add_prefix(self, prefix):
        new_name = prefix + self.base_name + "." + self.extension
        dst = os.path.join(self.path, new_name)
        os.rename(self.file_path, dst)
        self.file_path = dst
        return self


def _regex_text_search(text, regex):
    if regex:
        pattern = re.compile(regex, re.IGNORECASE)
        if pattern.search(text):
            return True
    else:
        return True
    return False


def _extension_match(file, ext):
    if ext:
        return file.extension == ext.lower().strip(".")
    else:
        return True
    return False


def file_search(path, regex=None, ext=None):
    """
    Generator that searches specified path and sub-paths for files by extension and/or pattern, returns File object
    """
    for root, dirs, files in os.walk(path):
        for filename in files:
            file = File(os.path.join(root, filename))

            regex_match = _regex_text_search(filename, regex)
            ext_match = _extension_match(file, ext)

            if all([regex_match, ext_match]):
                yield file
            else:
                continue


if __name__ == "__main__":
    pass
