import os
import shutil

ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__)))
TEST_DIR = os.path.join(ROOT_DIR, '_temp_test_files')
TEST_FILES = [
	"123.csv",
	"myfile.txt",
	"sub1/sub11/file202.md",
	"sub1/sub12/test_file_99.txt",
	"sub2/sub21/passwords.csv",
	"sub2/sub21/imagery_20190912.ecw",
	"sub2/sub21/metadata_20200122.txt",
	"sub3/sub31/sub311/test_file_88.csv",
	"sub3/sub32/sub321/junk.md",
	"sub3/sub33/geom.dbf",
]


def create_test_dir(filename):
	file_dir = os.path.join(TEST_DIR, os.path.dirname(filename))
	os.makedirs(file_dir, exist_ok=True)


def create_test_file(filename):
	if not os.path.isfile(filename):
		open(filename, "w+")


def create_test_set():
	if not os.path.exists(TEST_DIR):
		os.mkdir(TEST_DIR)
	for file in TEST_FILES:
		filename = os.path.join(TEST_DIR, file)
		create_test_dir(filename)
		create_test_file(filename)


def remove_test_set():
	shutil.rmtree(TEST_DIR)
