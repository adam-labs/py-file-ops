import pytest
import re
import os
from .context import files
from .generate_files import (
    TEST_DIR,
    create_test_set,
    remove_test_set
)

TEST_FILENAME = "test_file.txt"


@pytest.fixture
def test_file(tmp_path):
    file_path = tmp_path / TEST_FILENAME
    file_path.write_text("TEST CONTENT")
    f = files.File(file_path)
    return f


class TestFile:

    @staticmethod
    def check_path_prop(file, expected_file_path):
        file_path = os.path.dirname(expected_file_path)
        return file.path == str(file_path)

    @staticmethod
    def check_filename_prop(file, expected_file_path):
        expected_filename = os.path.basename(expected_file_path)
        return file.filename == expected_filename

    @staticmethod
    def check_base_name_prop(file, expected_file_path):
        expected_filename = os.path.basename(expected_file_path)
        expected_base_name = expected_filename.split(".")[0]
        return file.base_name == expected_base_name

    @staticmethod
    def check_extension_prop(file, expected_file_path):
        expected_filename = os.path.basename(expected_file_path)
        expected_extension = expected_filename.split(".")[1]
        return file.extension == expected_extension

    def check_props(self, file, expected_file_path):
        prop_check = [
            self.check_path_prop(file, expected_file_path),
            self.check_filename_prop(file, expected_file_path),
            self.check_base_name_prop(file, expected_file_path),
            self.check_extension_prop(file, expected_file_path),
        ]
        return all(prop_check)

    def test_props(self, tmp_path, test_file):
        file_path = os.path.join(tmp_path, TEST_FILENAME)
        assert self.check_props(test_file, file_path)

    def test_replace_non_alphanumeric(self, tmp_path, test_file):
        replace_char = "-"
        base_name, extension = TEST_FILENAME.split(".")
        filename = re.sub("[^0-9a-zA-Z]+", replace_char, base_name) + f".{extension}"
        file_path = os.path.join(tmp_path, filename)

        test_file.replace_non_alphanumeric(replace_char)
        assert self.check_props(test_file, file_path)

    def test_add_suffix(self, tmp_path, test_file):
        suffix = "_SUF"
        base_name, extension = TEST_FILENAME.split(".")
        filename = f"{base_name}{suffix}.{extension}"
        file_path = os.path.join(tmp_path, filename)

        test_file.add_suffix(suffix)
        assert self.check_props(test_file, file_path)

    def test_add_prefix(self, tmp_path, test_file):
        prefix = "PRE_"
        base_name, extension = TEST_FILENAME.split(".")
        filename = f"{prefix}{base_name}.{extension}"
        file_path = os.path.join(tmp_path, filename)

        test_file.add_prefix(prefix)
        assert self.check_props(test_file, file_path)


def test_regex_text_search():
    assert files._regex_text_search("Test_File_Name_ss_zz_123.txt", "Test_File_Name_ss_zz_123.txt")
    assert files._regex_text_search("Test_File_Name_ss_zz_123.txt", None)
    assert files._regex_text_search("Test_File_Name_ss_zz_123.txt", "")
    assert files._regex_text_search("Test_File_Name_ss_zz_123.txt", "test_file_name")
    assert files._regex_text_search("Test_File_Name_ss_zz_123.txt", "z{2}")
    assert files._regex_text_search("Test_File_Name_ss_zz_123.txt", "[^0-9a-zA-Z]+")
    assert not files._regex_text_search("Test_File_Name_ss_zz_123.txt", "notfound")


def test_extension_match(test_file):
    filename, extension = os.path.splitext(TEST_FILENAME)
    assert files._extension_match(test_file, extension)


class TestSearch:

    @classmethod
    def setup_class(cls):
        create_test_set()

    @classmethod
    def teardown_class(cls):
        remove_test_set()

    def test_filename_search(self):
        for file in files.file_search(TEST_DIR, regex='test_file_88.csv'):
            assert file.filename == 'test_file_88.csv'

    def test_regex_search(self):
        expected_files = [
            "123.csv",
            "file202.md",
            "test_file_99.txt",
            "imagery_20190912.ecw",
            "metadata_20200122.txt",
            "test_file_88.csv"
        ]
        for index, file in enumerate(files.file_search(TEST_DIR, regex="[0-9]")):
            assert file.filename == expected_files[index]

    def test_ext_search(self):
        expected_files = ["123.csv", "passwords.csv", "test_file_88.csv"]
        for index, file in enumerate(files.file_search(TEST_DIR, ext="csv")):
            assert file.filename == expected_files[index]

    def test_combined_search(self):
        expected_file = "test_file_88.csv"
        for file in files.file_search(TEST_DIR, regex="_+", ext='csv'):
            assert file.filename == expected_file

